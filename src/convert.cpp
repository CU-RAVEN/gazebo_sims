// Code that translate geo to angles for the gimbals
#include "ros/ros.h"
#include "sensor_msgs/NavSatFix.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/PoseStamped.h"
#include <math.h>

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    // TODO: Pass in parameter to select either VICON or GPS as source
    pub_tilt_ = n_.advertise<std_msgs::Float64>("/tilt_controller/command", 1000);
    pub_pan_ = n_.advertise<std_msgs::Float64>("/pan_controller/command", 1000);
    sub_ = n_.subscribe("/uav/fix", 1000, &SubscribeAndPublish::callback, this);
    sub_ugv_ = n_.subscribe("/navsat/fix", 1000, &SubscribeAndPublish::callback_ugv, this);
    sub_vicon_ = n_.subscribe("/relative_position", 1000, &SubscribeAndPublish::callback_vicon, this);
  }

  void callback_vicon(const geometry_msgs::PoseStamped& msg)
  {
  std_msgs::Float64 panHeading, tiltHeading;
  double x = msg.pose.position.x;
  double y = msg.pose.position.y;
  double z = msg.pose.position.z;

  panHeading.data  = atan2(e_uav, n_uav)+0.745514662912;
  tiltHeading.data = atan2(d_uav, sqrt((n_uav * n_uav) + (e_uav * e_uav)))-0.25157284921;
  if(panHeading.data < 0)
  {
	panHeading.data = 2*PI_+panHeading.data;
  }
  if(tiltHeading.data < 0)
  {
	tiltHeading.data = 2*PI_+tiltHeading.data;
  } else if(tiltHeading.data < 0)
  {
	panHeading.data = 0;
  } else if(tiltHeading.data > PI_)
  {
	panHeading.data = PI_;
  }
  pub_tilt_.publish(tiltHeading);
  pub_pan_.publish(panHeading);
  ros::spinOnce();
  ROS_INFO("X: %f", x);
  ROS_INFO("Y: %f", y);
  ROS_INFO("Z: %f\n", z);
  ROS_INFO("Pan: %f", panHeading.data);
  ROS_INFO("Tilt: %f\n", tiltHeading.data);
  }

  void callback(const sensor_msgs::NavSatFix& msg)
  {
  std_msgs::Float64 panHeading, tiltHeading;
  double lon = msg.longitude*PI_/180;
  double lat = msg.latitude*PI_/180;
  double alt = msg.altitude;

  double N_uav = a_ / sqrt(1 - e2_ * sin(lat) * sin(lat));
  double x_uav = (N_uav + alt) * cos(lat) * cos(lon);
  double y_uav = (N_uav + alt) * cos(lat) * sin(lon);
  double z_uav = ((1 - e2_) * N_uav + alt) * sin(lat);

  double x = x_uav-x_ugv;
  double y = y_uav-y_ugv;
  double z = z_uav-z_ugv;

  double n_uav = -sin(lat)*cos(lon)*x+-sin(lat)*sin(lon)*y+cos(lat)*z;
  double e_uav = -sin(lon)*x+cos(lon)*y;
  double d_uav = -cos(lat)*cos(lon)*x-cos(lat)*sin(lon)*y-sin(lat)*z;
  n_uav = n_uav;
  e_uav = -e_uav;
  d_uav = -d_uav;
  panHeading.data  = atan2(e_uav, n_uav)+0.745514662912;
  tiltHeading.data = atan2(d_uav, sqrt((n_uav * n_uav) + (e_uav * e_uav)))-0.25157284921;
  if(panHeading.data < 0)
  {
	panHeading.data = 2*PI_+panHeading.data;
  }
  if(tiltHeading.data < 0)
  {
	tiltHeading.data = 2*PI_+tiltHeading.data;
  } else if(tiltHeading.data < 0)
  {
	panHeading.data = 0;
  } else if(tiltHeading.data > PI_)
  {
	panHeading.data = PI_;
  }
  pub_tilt_.publish(tiltHeading);
  pub_pan_.publish(panHeading);
  ros::spinOnce();
  ROS_INFO("N: %f", x);
  ROS_INFO("E: %f", y);
  ROS_INFO("D: %f\n", z);
  ROS_INFO("Pan: %f", panHeading.data);
  ROS_INFO("Tilt: %f\n", tiltHeading.data);
  }

  void callback_ugv(const sensor_msgs::NavSatFix& msg)
  {
  double ugv_lon = msg.longitude*PI_/180;
  double ugv_lat = msg.latitude*PI_/180;
  double ugv_alt = msg.altitude;
  double N_ = a_ / sqrt(1 - e2_ * sin(ugv_lat) * sin(ugv_lat));
  x_ugv = (N_ + ugv_alt) * cos(ugv_lat) * cos(ugv_lon);
  y_ugv = (N_ + ugv_alt) * cos(ugv_lat) * sin(ugv_lon);
  z_ugv = ((1 - e2_) * N_ + ugv_alt) * sin(ugv_lat);
  }

private:
  ros::NodeHandle n_; 
  ros::Publisher pub_tilt_;
  ros::Publisher pub_pan_;
  ros::Subscriber sub_;
  ros::Subscriber sub_ugv_;
  ros::Subscriber sub_vicon_;
  double PI_ = 3.141592653589793238462643383279502884;
  double a_ = 6378137.0;
  double e2_ = 6.6943799901377997e-3;
  double x_ugv = 0; 
  double y_ugv = 0;
  double z_ugv = 0;
  

};

int main(int argc, char **argv)
{

  ros::init(argc, argv, "convert");
  SubscribeAndPublish SAPObject;

  ros::spin();

  return 0;
}
