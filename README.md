# RAVEN (Rover and Air Visual Environment Navigation)
# Gazebo Simulations Repository

## Overview

Contained in this repository are Gazebo world files, model definition files, and ROS launch files needed to launch a Gazebo world with a Clearpath Jackal UGV model, and a Hector UAV model.

## Dependencies and Setup

In order to run a simulation with a Jackal UGV and a Hector UAV, some packages have to be installed first. In

To download and build the Jackal packages for ROS Kinetic (which are only developed up to ROS Indigo), follow the tutorial [here](https://gist.github.com/vfdev-5/57a0171d8f5697831dc8d374839bca12). The following instructions assume you have already created a catkin workspace, which I will refer to here as `catkin_ws`. Replace this with whatever the name of your workspace is.
Next, get the Hector packages:

```
$ cd ~/catkin_ws/src
$ git clone https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor.git
```

Before building your workspace, install the following packages: hector-pose-estimation, hector-sensor-description, message-to-tf.

```
$ sudo apt-get update
$ sudo apt-get install ros-kinetic-hector-pose-estimation ros-kinetic-hector-sensor-description ros-kinetic-message-to-tf
```
This is most likely a path issue instead of not having the package, but this will solve the problem for the time being.

Build your workspace:
```
$ cd ~/catkin_ws
$ catkin_make
```

## Running

In order to launch the world, run the command

`$ roslaunch gazebo_sims ugv_uav.launch`

This will launch the gazebo world. Still in development is the control of both vehicles.

To start the UAV, and then make it fly in a circle around the UGV, run:
```
$ rosservice call /enable_motors "enable: true"
$ rosrun gazebo_sims pose_commander
```

The UGV should take off and orbit the UGV.

**Known Bug**: Sometimes, when the pose_commander node is run, it will crash immediately due to the UGV postion transform not being available. Try to run it a couple times, and it should work. Fix in progress.

Driving the UGV requires a wired game controller. Only a wired Logitech game controller has been tested, but the Jackal code downloaded following the linked tutorial above includes code for using an XBox or PlayStation wired controller as well. YMMV.

Once the controller is plugged in, depress the left joystick and, while keeping the stick depressed, move it forward or backwards to drive the UGV, and left or right to turn the UGV.